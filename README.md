# Car Report

## Description

Car Report is an android app, which lets you enter refuelings and other costs of your cars
and displays nice reports. The following are currently included:

1. Fuel consumption
1. Fuel price
1. Mileage
1. Costs in general

It provides basic synchronization with Dropbox and Google Drive and has basic backup/restore
and CSV import/export functionality.

## Get it

[Car Report on Play Store](https://play.google.com/store/apps/details?id=me.kuehle.carreport)

## Used libraries

* [Joda Time](http://joda-time.sourceforge.net)
* [HoloColorPicker](https://github.com/LarsWerkman/HoloColorPicker)
* [ChartLib](https://bitbucket.org/frigus02/chartlib)
* [ActiveAndroid](https://github.com/pardom/ActiveAndroid)
* [DragSortListView](https://github.com/bauerca/drag-sort-listview)